using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [Header("Setting SpawnEnemy")]
    public GameObject enemyPrefab; // Prefab ของ Enemy
    public int numberOfEnemy = 5; // จำนวนของ Enemy ที่ต้องการให้เกิด
    public float timeSpawns = 2f; // เวลาระหว่างการเกิด Enemy
    
    public Transform[] spawnPoints; // ตำแหน่งที่ Enemy จะเกิดขึ้น

    IEnumerator Start()
    {
        yield return StartCoroutine(SpawnEnemies());
    }

    IEnumerator SpawnEnemies()
    {
        for (int i = 0; i < numberOfEnemy; i++)
        {
            Transform randomSpawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)]; // เลือกตำแหน่งสุ่มที่จะเกิด Enemy
            Instantiate(enemyPrefab, randomSpawnPoint.position, randomSpawnPoint.rotation); // เกิด Enemy ที่ตำแหน่งที่สุ่มได้

            yield return new WaitForSeconds(timeSpawns); // รอเวลาระหว่างการเกิด Enemy
        }
    }
}
