using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharacterSelection : MonoBehaviour
{
    public GameObject[] characterModels;
    public TextMeshProUGUI selectedCharacterText;
    
    [Header("Animator")]
    public Animator SwordAnimator;  // ตัวแปร Animator
    public Animator MageAnimator; 
    
    private int currentCharacterIndex = 0;
    
    void Start()
    {
        UpdateSelectedCharacter();
    }

    public void SelectNextCharacter()
    {
        characterModels[currentCharacterIndex].SetActive(false);
        currentCharacterIndex = (currentCharacterIndex + 1) % characterModels.Length;
        characterModels[currentCharacterIndex].SetActive(true);
        UpdateSelectedCharacter();
        
        SwordAnimator.SetTrigger("MoveMenu");
    }

    public void SelectPreviousCharacter()
    {
        characterModels[currentCharacterIndex].SetActive(false);
        currentCharacterIndex = (currentCharacterIndex - 1 + characterModels.Length) % characterModels.Length;
        characterModels[currentCharacterIndex].SetActive(true);
        UpdateSelectedCharacter();
        
        MageAnimator.SetTrigger("MoveMenu");
    }

    void UpdateSelectedCharacter()
    {
        selectedCharacterText.text = "Players: " + characterModels[currentCharacterIndex].name;
        PlayerPrefs.SetInt("Players", currentCharacterIndex);

    }

    public void SelectCharacterLoad()
    {
        // ตรวจสอบตัวละครที่เลือก
        switch (currentCharacterIndex)
        {
            case 0: // Player SwordAnimator
                SceneManager.LoadScene("SwordScene", LoadSceneMode.Single);
                break;

            case 1: // Player MageAnimator
                SceneManager.LoadScene("MageScene", LoadSceneMode.Single);
                break;

            // ตัวเลือกอื่น ๆ ที่คุณต้องการเพิ่ม

            default:
                Debug.LogError("Invalid character selection");
                break;
        }
    }
}