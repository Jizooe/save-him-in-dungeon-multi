using System;
using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Security;
using TMPro;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class BossAttacks : MonoBehaviour
{
    [Header("Control")] public bool AI;
    public bool debug;

    [Header("References")] public Transform model;
    public Transform player; //ตัวแปร Target
    public BoxCollider leftFootCollider;
    public Transform spellPosition;
    public Transform impactPosition;
    private Animator playerAnim;

    public DamageDealer greatSword;
//    public CameraShaker shaker;
//    public GameManagerScript gameManager; 

    [Header("Attacks")] public GameObject earthShatterPrefab;
    public GameObject magicSwordFromSky;
//    public GameObject spell;
    public GameObject auraMagic;
    public GameObject screamMagic;
    public GameObject magicFarSword;
    public GameObject impactPrefab;

    private Animator anim;

    [Header("Debug")] public GameObject brainIcon;
    public Image bossAttackingDebug;
    public Image bossMovingDebug;
    public TextMeshProUGUI walkTimeDebug;
    public TextMeshProUGUI distanceDebug;
    public TextMeshProUGUI brainDebug;
    public TextMeshProUGUI damageDebug;
    public TextMeshProUGUI speedText;
    public Color farColor;
    public Color middleColor;
    public Color nearColor;

    [Header("AI Manager")] public float nearValue;
    public float farValue;
    public float chillTime;
    private string action;
    private float lastActionTime;
    private float distance = 100; //ปรับเป็น 100 เพื่อไม่ให้เริ่มเกมมา มันเท่ากับ 0 
    private float chillDirection;
    private bool phase2;
    private bool canBeginAI; // รอเวลาสักพักก่อนโจมตีครั้งแรก
    private int lastAttack; // บันทึกการโจมตีครั้งสุดท้าย
    private bool EnemyAwake;

    // SlowBossDown
    private bool slowDown;
    private string actionAfterSlowDown;

    private GameManagerScript _gameManagerScript;
    private RPCAnimationBoss _rpcAnimationBoss;


    private void Awake()
    {
        anim = model.GetComponent<Animator>();
        
    }

    private void Start()
    {
        //    anim = model.GetComponent<Animator>();
        //    playerAnim = player.GetComponent<Animator>();
        _gameManagerScript = GameManagerScript.Instance;
        _gameManagerScript.Enemy.Add(transform);
        _rpcAnimationBoss = GetComponent<RPCAnimationBoss>();


        Vector3 size = new Vector3(0.8077834f, 0.6475059f, 2.036553f); // ขนาดดาบใหญ่
        Vector3 center = new Vector3(0.01120045f, 0.02619396f, -0.4916435f);
        SetGreatSwordSize(size, center);
    }


    private void OnDestroy()
    {
        _gameManagerScript.Enemy.Remove(transform);
    }

    protected virtual void Update()
    {

        Bossdistance(); //เรียกระยะห่างboss กับ playerในเกม

        DebugAttack();
        speedText.text = anim.GetFloat("Vertical").ToString("0.0");

        if (Input.GetKeyDown(KeyCode.Keypad0) && _gameManagerScript.master) AI = !AI;
        if (Input.GetKeyDown(KeyCode.Keypad1) && _gameManagerScript.master) debug = true;
        brainIcon.gameObject.SetActive(AI);

        this.transform.position = new Vector3(transform.position.x, 0, transform.position.z);

        if (debug)
            DebugUI();

        seePlayer();

        seeDebugs();

    }

    IEnumerator StartAI()
    {
        yield return new WaitForSeconds(4);
        canBeginAI = true;
    }

    public void DebugUI()
    {
        speedText.transform.parent.gameObject.SetActive(true);
        distanceDebug.transform.parent.gameObject.SetActive(true);
        brainDebug.transform.parent.gameObject.SetActive(true);
        damageDebug.text = greatSword.damageAmount.ToString();
        bossAttackingDebug.gameObject.SetActive(anim.GetBool("Attacking"));
        bossMovingDebug.gameObject.SetActive(action == "Move");
        distanceDebug.text = distance.ToString("0.0"); // แสดงระยะทางในการดีบัก
        if (distance <= nearValue) distanceDebug.color = nearColor;
        else if (distance >= farValue) distanceDebug.color = farColor;
        else distanceDebug.color = middleColor;
    }

    public void FarAttack(int rand = 0)
    {
        brainDebug.text = "Far Attack";
        anim.SetFloat("Vertical", 0);
        anim.SetFloat("Horizontal", 0);

        object[] data1 = new object[]
        {
            "Vertical", 0.0f,
            "Horizontal", 0.0f
        };
        _rpcAnimationBoss.AnimationMovementBossRPC(data1);

//        int rand = 0;
        do
        {
            if (!anim.GetBool("Phase2")) rand = Random.Range(0, 7);
            if (anim.GetBool("Phase2")) rand = Random.Range(0, 8);
            
            //RPC
            object[] data0 = new object[]
            {
                "Phase2", false
            };
            _rpcAnimationBoss.AnimationBoolBossRPC(data0);
            
            //RPC
            object[] data00 = new object[]
            {
                "Phase2", true
            };
            _rpcAnimationBoss.AnimationBoolBossRPC(data00);
            
        } while (rand == lastAttack);

        lastAttack = rand;

        if (anim.GetBool("Phase2") && Random.Range(0, 2) == 0)
        {
//            anim.SetTrigger("Spell"); // Fireball

            //RPC
//            _rpcAnimationBoss.AnimationAttackBossRPC("Spell");
        }

        skillFarAttack(rand); 

        action = "Wait"; // impede que essa acao seja executada novamente
    }

    public void skillFarAttack(int rand) //เรียกใช้ RPC และนำไปฝากใช้ในแทน switch อันเดิม
    {
        switch (rand)
        {
            case 0:
                anim.SetTrigger("CastMagicSwords"); // Magic swords from sky
                break;
            case 1:
                anim.SetTrigger("Casting"); // Earth Shatter
                break;
            case 2:
                anim.SetTrigger("Dash");
                break;
            case 3:
                anim.SetTrigger("DoubleDash");
                break;
            case 4:
//                anim.SetTrigger("Spell"); // Fireball
                break;
            case 5:
                anim.SetTrigger("Scream");
                break;
            case 6:
                anim.SetTrigger("Fishing"); // Magic Far Sword
                break;
            case 7:
                anim.SetTrigger("SuperSpinner");
                break;
            default:
                break;
        }

        //RPC
        _rpcAnimationBoss.AnimationByteRPC(rand); //เรียกใช้ enum rand จะส่งค่าไปs
    }

public enum AttackBoss //เอาไปใส่ในฝั่งรับ
    {
        CastMagicSwords = 0,
        Casting = 1,
        Dash = 2,
        DoubleDash = 3,
        Spell = 4,
        Scream = 5,
        Fishing = 6,
        SuperSpinner = 7,
    }

    public void NearAttack(int rand = 0)
    {
        anim.SetFloat("Vertical", 0);
        anim.SetFloat("Horizontal", 0);
        
        object[] data1 = new object[]
        {
            "Vertical", 0.0f,
            "Horizontal", 0.0f
        };
        _rpcAnimationBoss.AnimationMovementBossRPC(data1);

//        int rand = 0;

        do //ทำloop 
        {
            if (!anim.GetBool("Phase2")) rand = Random.Range(0, 10);
            if (anim.GetBool("Phase2")) rand = Random.Range(0, 13);
            
            //RPC
            object[] data0 = new object[]
            {
                "Phase2", false
            };
            _rpcAnimationBoss.AnimationBoolBossRPC(data0);
            
            //RPC
            object[] data00 = new object[]
            {
                "Phase2", true
            };
            _rpcAnimationBoss.AnimationBoolBossRPC(data00);
            
        } while (rand == lastAttack);  //เช็คการสุ่มท่าของบอสให้ไม่ซ้ำกับท่าที่แล้ว 
        lastAttack = rand;

        skillNearAttack(rand);

    }

    public void skillNearAttack(int rand)
    {
        switch (rand)
        {
            case 0:
                anim.SetTrigger("DoubleDash");
                brainDebug.text = "Double Dash";
                break;
            case 1:
                anim.SetTrigger("Dash");
                brainDebug.text = "Dash";
                break;
            case 2:
                anim.SetTrigger("SpinAttack");
                brainDebug.text = "Spin Attack";
                break;
            case 3:
                anim.SetTrigger("Combo");
                brainDebug.text = "Combo";
                break;
            case 4:
                anim.SetTrigger("Casting");
                brainDebug.text = "Casting";
                break;
            case 5:
                anim.SetTrigger("Combo1");
                brainDebug.text = "Combo1";
                break;
            case 6:
//                anim.SetTrigger("Spell");
//                brainDebug.text = "Spell";
                break;
            case 7:
                anim.SetTrigger("AuraCast");
                brainDebug.text = "Aura Cast";
                break;
            case 8:
                anim.SetTrigger("ForwardAttack");
                brainDebug.text = "ForwardAttack";
                break;
            case 9:
                anim.SetTrigger("Scream");
                brainDebug.text = "Scream";
                break;
            case 10:
                anim.SetTrigger("Impact");
                brainDebug.text = "Impact";
                break;
            case 11:
                anim.SetTrigger("Strong");
                brainDebug.text = "Strong";
                break;
            case 12:
                anim.SetTrigger("JumpAttack");
                brainDebug.text = "Jump Attack";
                break;
            default:
                break;
        }
        
        //RPC
        _rpcAnimationBoss.AnimationByteRPC(rand); //เรียกใช้ enum rand จะส่งค่าไป
    }

    public void SlowBossDown()
    {
        if (anim.GetFloat("Vertical") <= 0.4f)
        {
            slowDown = false;
            if (actionAfterSlowDown == "CallNextMove")
            {
                action = "Wait";
                anim.SetFloat("Vertical", 0);
                anim.SetFloat("Horizontal", 0);
                
                object[] data = new object[]
                {
                    "Vertical", 0.0f,
                    "Horizontal", 0.0f
                };
                _rpcAnimationBoss.AnimationMovementBossRPC(data);
                
                StartCoroutine(WaitAfterNearMove());
            }
            else if (actionAfterSlowDown == "FarAttack")
            {
                action = "FarAttack";
            }
            else
            {
                Debug.LogError("Not supposed to be here");
            }
        }
        else
        {
            brainDebug.text = "SlowDown";
            anim.SetFloat("Vertical", Mathf.Lerp(anim.GetFloat("Vertical"), 0, 1 * Time.deltaTime));
        }
    }

    IEnumerator WaitAfterNearMove()
    {
        brainDebug.text = "WaitRandomly";
        slowDown = false;
        action = "Wait";
        anim.SetFloat("Vertical", 0);
        anim.SetFloat("Horizontal", 0);
        
        object[] data = new object[]
        {
            "Vertical", 0.0f,
            "Horizontal", 0.0f
        };
        _rpcAnimationBoss.AnimationMovementBossRPC(data);
        
        float maxWaitTime = 6;
        float possibility = 2;
        if (anim.GetBool("Phase2"))
        {
            maxWaitTime = 5.5f;
            possibility = 2;
        }
        float waitTime;
        float decision = Random.Range(0, possibility);
        if (decision == 0) waitTime = Random.Range(2.5f, maxWaitTime); 
        else waitTime = 0;
        yield return new WaitForSeconds(waitTime); 
        action = "NearAttack";
        CallNextMove();
    }

    public void MoveToPlayer() // bossเคลื่อนที่เข้าหาplayer
    {
        brainDebug.text = "Move";

        anim.SetFloat("Horizontal", 0); // บอสจะไม่ขยับไปด้านข้างขณะมุ่งหน้าไปยังผู้เล่น

        float speedValue = distance / 15; 
        if (speedValue > 1) speedValue = 1; 
        

        walkTimeDebug.text = (Time.time - lastActionTime).ToString("0.0");

        if (slowDown)
        {
            SlowBossDown();
            return;
        }

        if (distance < nearValue)
        {
            //anim.SetFloat("Vertical", 0);
            //CallNextMove();
            
            actionAfterSlowDown = "CallNextMove";
            slowDown = true;
        }
        else if (Time.time - lastActionTime > chillTime) 
        {
            //anim.SetFloat("Vertical", 0);
            //action = "FarAttack";
            
            actionAfterSlowDown = "FarAttack";
            slowDown = true;
        }
        else
        {
            anim.SetFloat("Vertical", speedValue); 
        }
    }

    public void WaitForPlayer()
    {
        brainDebug.text = "Chill";

        anim.SetFloat("Horizontal", chillDirection);
        anim.SetFloat("Vertical", 0);

        if ((distance <= nearValue && Time.time - lastActionTime > chillTime) && !phase2) 
        {
            CallNextMove();
        } else

        if((distance > farValue && Time.time - lastActionTime > chillTime) && !phase2) 
        {
            FarAttack();
        } else

        if ((Time.time - lastActionTime > chillTime) || phase2 && Time.time - lastActionTime > chillTime)
        {
            int rand = Random.Range(0, 3);

            if (rand % 2 == 0)
            {
                NearAttack();
            }
            else if (rand % 2 == 1)
            {
                FarAttack();
            }
        }

    }

    //บอสล่า
    public void AI_Manager()
    {
        if (action == "Wait" || anim.GetBool("Dead") || anim.GetBool("Transposing")) return; 

        if (action == "Move")
        {
            MoveToPlayer(); // ย้ายไปที่ผู้เล่น
        }

        if(action == "WaitForPlayer")
        {
            WaitForPlayer(); // เดินรอให้Playerเข้าใกล้
        }

        if(action == "FarAttack")
        {
                FarAttack(); // ทำการโจมตีระยะไกลครั้งเดียว
        }

        if(action == "NearAttack")
        {
            if (!anim.GetBool("TakingDamage"))
            {
                NearAttack(); // ทำการโจมตีระยะสั้น
            }
        }
    }

    public void CallNextMove()
    {
        lastActionTime = Time.time;

        if (distance >= farValue && !anim.GetBool("Dead")) 
        {
            action = "Move";
        } 
        else if (distance > nearValue && distance < farValue && !anim.GetBool("Dead")) 
        {
            int rand = Random.Range(0, 2);
            if (rand == 0) chillDirection = -0.5f;
            if (rand == 1) chillDirection = 0.5f;
            action = "WaitForPlayer";
        }
        else if (distance <= nearValue && !anim.GetBool("Dead"))
        {
            action = "NearAttack";
        }
    }

    public bool IsBossTakingDamage()
    {
        return !anim.GetCurrentAnimatorStateInfo(2).IsName("none");
    }

    #region Debug

    public void DebugAttack()
    {
        anim.SetFloat("Vertical", 0);

        if (Input.GetKeyDown(KeyCode.T))
        {
            anim.SetTrigger("AuraCast");
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            anim.SetTrigger("Spell");
        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            anim.SetTrigger("Impact");
        }

        if (Input.GetKeyDown(KeyCode.Y))
        {
            anim.SetTrigger("SpinAttack");
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            anim.SetTrigger("Casting");
        }

        if (Input.GetKeyDown(KeyCode.N))
        {
            anim.SetTrigger("Strong");
        }

        if (Input.GetKeyDown(KeyCode.U))
        {
            anim.SetTrigger("CastMagicSwords");
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            anim.SetTrigger("SuperSpinner");
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            anim.SetTrigger("JumpAttack");
        }

        if (Input.GetKeyDown(KeyCode.I))
        {
            anim.SetTrigger("ForwardAttack");
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            anim.SetTrigger("Scream");
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            anim.SetTrigger("Fishing");
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            anim.SetTrigger("Combo");
        }

    }

    #endregion

    #region Magics
    public void SpawnEarthShatter() //เรียกท่า ตีลงพื้นฟ้า
    {
        Vector3 bossPos = model.transform.position;
        Vector3 bossDirection = model.transform.forward;
        Quaternion bossRotation = model.transform.rotation;
        float spawnDistance = 3;

        Vector3 spawnPos = bossPos + bossDirection * spawnDistance;
        GameObject earthShatter = Instantiate(earthShatterPrefab, spawnPos, Quaternion.identity);
        earthShatter.transform.rotation = bossRotation;
        Destroy(earthShatter, 4);
        
        object[] data = new object[]
        {
            "  "
        };
        _rpcAnimationBoss.AnimationStringRPC(data);

//        shaker.ShakeCamera(1.5f);  //กล้องสั่น
    }

    public void Scream()
    {
        GameObject scream = Instantiate(screamMagic, model.transform.position, Quaternion.identity);
        scream.transform.eulerAngles = new Vector3(90, 0, 0);
        
        object[] data = new object[]
        {
            "  "
        };
        _rpcAnimationBoss.AnimationStringRPC(data);
    }

    public void SwordsFromSkyAttack() 
    {
        StartCoroutine(DropSwordsFromSky(15));
        
        object[] data = new object[]
        {
            "  "
        };
        _rpcAnimationBoss.AnimationStringRPC(data);
    }

    IEnumerator DropSwordsFromSky(int counter)
    {
        float x_offset = Random.Range(-1, 1); 
        float z_offset = Random.Range(-1, 1); 
        GameObject earth = Instantiate(magicSwordFromSky, new Vector3(player.transform.position.x + x_offset, player.transform.position.y + 6, player.transform.position.z + z_offset), Quaternion.identity);
        yield return new WaitForSeconds(0.25f);
        if (counter > 0) 
            StartCoroutine(DropSwordsFromSky(counter - 1));
        
        object[] data = new object[]
        {
            "  "
        };
        _rpcAnimationBoss.AnimationStringRPC(data);
    }

    public void CastAura() 
    {
        if (!IsBossTakingDamage())
        { 
            Vector3 spawnPos = model.transform.position;
            spawnPos.y = 0.02f;
            GameObject aura = Instantiate(auraMagic, spawnPos, Quaternion.identity);
            aura.transform.eulerAngles = new Vector3(-90, 0, 0);
            aura.transform.position += new Vector3(0, 0.2f, 0);
            
            object[] data = new object[]
            {
                "  "
            };
            _rpcAnimationBoss.AnimationStringRPC(data);
            
        }
    }
/*
    public void FireSpell() 
    {
        if (!IsBossTakingDamage())
        {
            Vector3 relativePos = player.position - spellPosition.position;
            Instantiate(spell, spellPosition.position, Quaternion.LookRotation(relativePos, Vector3.up));
            
            object[] data = new object[]
            {
                "  "
            };
            _rpcAnimationBoss.AnimationStringRPC(data);
        }
    }*/ //ไปใส่ Particleมา

    public void Impact() 
    {
        GameObject impactObj = Instantiate(impactPrefab, impactPosition.position, Quaternion.identity);
        Destroy(impactObj, 1.5f);
        
        object[] data = new object[]
        {
            "  "
        };
        _rpcAnimationBoss.AnimationStringRPC(data);
        
//        shaker.ShakeCamera(0.5f);
    }

    public void LightGreatSwordUp()
    {
        greatSword.gameObject.GetComponent<GreatSwordScript>().EnableGreatSwordFire(); // เปิดใช้งานไฟ GreatSword แต่ยังไม่ได้ทำ 

        Vector3 size = new Vector3(0.00075f, 0.0004f, 0.018f);
        Vector3 center = new Vector3(0f, 0f, 0.009f);
        SetGreatSwordSize(size, center);
        greatSword.gameObject.GetComponent<GreatSwordScript>().customSize += new Vector3(0, 0, 0.012f);
        
        object[] data = new object[]
        {
            "  "
        };
        _rpcAnimationBoss.AnimationStringRPC(data);
    }
    public void SetGreatSwordSize(Vector3 size, Vector3 center) // เปลี่ยนขนาดของ hitbox ของ GreatSword
    {
        greatSword.gameObject.GetComponent<BoxCollider>().size = size;
        greatSword.gameObject.GetComponent<BoxCollider>().center = center;
        
        object[] data = new object[]
        {
            "  "
        };
        _rpcAnimationBoss.AnimationStringRPC(data);
    }

    public void MagicFarSword()
    {
        GameObject obj = Instantiate(magicFarSword, greatSword.transform.position, Quaternion.identity);
        Destroy(obj, 4.5f);
        
        object[] data = new object[]
        {
            "  "
        };
        _rpcAnimationBoss.AnimationStringRPC(data);
    }

    #endregion

    #region Kick

    public void TurnKickColliderOn()
    {
        leftFootCollider.enabled = true;
        leftFootCollider.GetComponent<DamageDealer>().damageOn = true;
    }

    public void TurnKickColliderOff()
    {
        leftFootCollider.enabled = false;
        leftFootCollider.GetComponent<DamageDealer>().damageOn = false;
    }

    #endregion

    public void SetNotAttackingFalse() // เมื่อเริ่มภาพเคลื่อนไหวการโจมตีแต่ละครั้ง
    {
        anim.SetBool("NotAttacking", false);
        
        //RPC
        object[] data = new object[]
        {
            "NotAttacking", false
        };
        _rpcAnimationBoss.AnimationBoolBossRPC(data);
    }

    public void SetNotAttackingTrue() // กำหนดตามชื่อและตอนจบของแอนิเมชันบางส่วน
    {
        anim.SetBool("NotAttacking", true);
        
        //RPC
        object[] data = new object[]
        {
            "NotAttacking", true
        };
        _rpcAnimationBoss.AnimationBoolBossRPC(data);
    }

    public void SetCanRotateTrue() // บอสสามารถมองผู้เล่นได้
    {
        anim.SetBool("CanRotate", true);
        
        //RPC
        object[] data = new object[]
        {
            "CanRotate", true
        };
        _rpcAnimationBoss.AnimationBoolBossRPC(data);
    }

    public void SetCanRotateFalse()
    {
        anim.SetBool("CanRotate", false);
        
        //RPC
        object[] data = new object[]
        {
            "CanRotate", false
        };
        _rpcAnimationBoss.AnimationBoolBossRPC(data);
    }
    
    public Transform Bossdistance() 
    {
        if (_gameManagerScript.player.Count > 0) //เพิ่มมา คู่กับ Gamemanager
        {
            float currentDistance = 1000;
            foreach (var playerTransform in _gameManagerScript.player)
            {
                float playerDistance = Vector3.Distance(transform.position, playerTransform.position);

                if (playerDistance < currentDistance) //ทั้งหมดนี่ เช็ค bossกับplayerทั้งสองให้ระยะเท่ากัน
                {
                    currentDistance = playerDistance;

                    Debug.Log("1 Player");
                    player = playerTransform;
                }
            }

            distance = currentDistance;
            Debug.Log("ตัวที่สอง");
            Debug.Log("distance " + distance);
        }

        return player;
    }

    public void seePlayer()
    {
        if (distance < 20 && !anim.GetBool("Equipped"))  //ระยะน้อยกว่า 20 และถืออาวุธน้า
        {
            anim.SetTrigger("DrawSword");
            StartCoroutine(StartAI());
            
            //RPC
            object[] data = new object[]
            {
                "DrawSword", true
            };
            _rpcAnimationBoss.AnimationAttackBossRPC("DrawSword");
        }
    }

    public void seeDebugs()
    {
        if (!anim.GetBool("Equipped")) return;

        if (!canBeginAI) return;
        {
            if (player != null)
            {
                playerAnim = player.GetComponent<Animator>();

                if (AI && !playerAnim.GetBool("Dead"))
                {
                    AI_Manager();

                }
                else if (_gameManagerScript.master)
                {
                    DebugAttack();
                }
                else
                {
                    anim.SetBool("GameEnd", true);
                    anim.SetBool("CanRotate", false);
                }
            }
        }

        greatSword.damageOn = anim.GetBool("Attacking"); 
        //RPC
        object[] data = new object[]
        {
            "Attacking", true
        };
        _rpcAnimationBoss.AnimationBoolBossRPC(data);

        phase2 = anim.GetBool("Phase2"); 
        //RPC
        object[] data1 = new object[]
        {
            "Phase2", true
        };
        _rpcAnimationBoss.AnimationBoolBossRPC(data1);
    }

}
