using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class LifeBarScript : MonoBehaviour
{
    private float life = 10; 
    private float ghost = 10; 
    public Animator playerAnim;

    public Image lifeBar;
    public Image lifeGhost; 

    private float lastTime;
    private float waitTime = 1.5f;

    private bool mouseInputEnabled = false;
    public GameObject youDiedScreen;
    
    private float bleeding;

    private bool SloDownTime;
    private float journeyLength = 15;
    private float startTime = -1;


    public GameManagerScript gameManager;
    public BossLifeBarScript bossLifeManager;

    

    private void Start()
    {
        lifeBar.rectTransform.sizeDelta = new Vector2(life * 100, 24);
        lifeGhost.rectTransform.sizeDelta = new Vector2(life * 100, 24);

//        gameManager.playerIsDead = false;
        
    }

    private void FixedUpdate()
    {
        if (SloDownTime && Time.timeScale > 0.5f) // ผู้เล่นเสียชีวิต ทำให้เวลาช้าลง
        {
            Debug.Log("a");
            if(startTime <= 0)
                startTime = Time.time;
            float distCovered = (Time.time - startTime) * 0.1f;
            float fractionOfJourney = distCovered / journeyLength;
            Time.timeScale = Mathf.Lerp(Time.timeScale, 0.5f, fractionOfJourney);   
        }

        if (life > ghost) //เช็คขนาด ถ้า Life มากกว่า Ghost
        {
            ghost = life;
            lifeGhost.rectTransform.sizeDelta = new Vector2(ghost * 100, 24);
        }

        if (Time.time > lastTime + waitTime && ghost > life) //ค่อยลดหลอghost
        {
            ghost -= 0.1f;
            lifeGhost.rectTransform.sizeDelta = new Vector2(Mathf.Lerp(ghost, life, 5 * Time.deltaTime) * 100, 24);
        }
/*        
        if(playerAnim.GetBool("Dead")) //เปิดใช้งานเมื่อตาย
        {
            youDiedScreen.SetActive(true);
        }
*/        
    }

    public void UpdateLife(float amount)
    {
        if (amount < 0) 
        {
            lastTime = Time.time;
        } 
        else 
        {
            StopAllCoroutines(); 
        }

        life += amount;

        if (life > 10) life = 10; 
        if (life < 0) life = 0;

        if (life == 0 && !playerAnim.GetBool("Dead"))
        {
            Die();
        }

        lifeBar.rectTransform.sizeDelta = new Vector2(life * 100, 24); //อัพเดตขนาดแถบสุขภาพ
    }

    public void StartBleeding() 
    {
        if (IsDead()) return; 
        bleeding = 400;
        StartCoroutine(Burning(10));
    }

    IEnumerator Burning(int cicles)
    {
        yield return new WaitForSeconds(1f);
        if(cicles > 0)
        {
            UpdateLife(-0.2f);
            bleeding -= 40;

            StartCoroutine(Burning(cicles - 1));
        }
    }

    private void Die()
    {
        playerAnim.SetFloat("Vertical", 0); 
        playerAnim.SetFloat("Horizontal", 0);
        playerAnim.SetFloat("Speed", 0);
        playerAnim.SetTrigger("DieForward"); 
        playerAnim.SetBool("Dead", true); 
        youDiedScreen.SetActive(true); 
        if (!mouseInputEnabled) 
        {
            EnableMouseInput(); //เรียกใช้เมาส์
        }
        playerAnim.gameObject.GetComponent<IKFootPlacement>().SetIntangibleOn();

        gameManager.playerIsDead = true; // playerIsDead เป็นจริง
        
    }


    IEnumerator WaitToRestart()
    {
        yield return new WaitForSeconds(2);
        gameManager.Restart();
    }

    public bool IsDead()
    {
        return playerAnim.GetBool("Dead");
    }

    public bool GetNoDamageTaken()
    {
        return life == 10;
    }

    private void EnableMouseInput()
    {
        mouseInputEnabled = true;
        Cursor.lockState = CursorLockMode.None;  // เปิดการใช้งานการเคลื่อนไหวของเมาส์
        Cursor.visible = true;  // ทำให้เมาส์ปรากฏบนหน้าจอ
    }

}
