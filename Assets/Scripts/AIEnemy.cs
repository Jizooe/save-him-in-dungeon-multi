using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class AIEnemy : MonoBehaviour
{
    private GameObject player;
    private GameObject player2;
    private NavMeshAgent agent;

    [SerializeField] private LayerMask groundLayer, playerLayer;

    Animator anim;

    private BoxCollider boxCollider;
    
    [Header("Patrol")]
    private Vector3 destPoint;
    private bool walkpointSet;
    
    [Header("State change")]
    [SerializeField] private float range; // รัศมีการเดินตระเวน
    [SerializeField] private float cansee; //การมองเห็น
    [SerializeField] private float attackRange;  //ระยะโจมตี
    bool playerInSinght;
    bool playerInAttackRang;

    [Header("Enemy Attack")]
    public int attackDamage = 10; // ค่าการโจมตีของ AIEnemy
    
    [Header("Setting HP")]
    public int healthLevel = 10;
    public int maxHealth;
    public int currentHealth;
    
    public HealthBar healthBar;
    

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.Find("P_Player");
        player2 = GameObject.Find("P_Player2");
        anim = GetComponent<Animator>();
        boxCollider = GetComponentInChildren<BoxCollider>();
        
        maxHealth = SetMaxHealthFromHealthLevels();
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
        
//        healthBar = GameObject.Find("Health Bar").GetComponent<HealthBar>();
        
        // Call SearchForDest() at the start to set the initial destination
        SearchForDest();
    }
    
    private int SetMaxHealthFromHealthLevels()
    {
        maxHealth = healthLevel * 10;
        return maxHealth;
    }

    void Update()
    {
        playerInSinght = Physics.CheckSphere(transform.position, cansee, playerLayer);
        playerInAttackRang = Physics.CheckSphere(transform.position, attackRange, playerLayer);
        
        if(!playerInSinght && !playerInAttackRang) Patrol();
        if(playerInSinght && !playerInAttackRang) Chase();
        if(playerInSinght && playerInAttackRang) Attack();
        
        
    }

    void Chase()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        if (players.Length > 0)
        {
            GameObject closestPlayer = FindClosestPlayer(players);

            if (closestPlayer != null)
            {
                if (playerInSinght && !playerInAttackRang)
                {
                    agent.SetDestination(closestPlayer.transform.position);
                }
            }
        }
    }

    GameObject FindClosestPlayer(GameObject[] players)
    {
        GameObject closestPlayer = null;
        float closestDistance = float.MaxValue;

        foreach (GameObject player in players)
        {
            float distance = Vector3.Distance(transform.position, player.transform.position);

            if (distance < closestDistance)
            {
                closestPlayer = player;
                closestDistance = distance;
            }
        }

        return closestPlayer;
    }



    void Attack()
    {
        if (anim != null)
        {
            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("AttackEnemy"))
            {
                anim.SetTrigger("Attack");
                agent.SetDestination(transform.position);
                
                    //Sword
                    if (player != null)
                    {
                        var playerController = player.GetComponent<PlayerController>();
                    
                        if (playerController != null)
                        {
                            playerController.TakeDamage(attackDamage);
                        }
                    }
                    
                    //Mage
                    if (player2 != null)
                    {
                        var player2Controller = player2.GetComponent<Player2Controller>();
                    
                        if (player2Controller != null)
                        {
                            player2Controller.TakeDamage(attackDamage);
                        }
                    }
                }
            }
    }

    
    void Patrol()
    {
        if (!walkpointSet)
        {
            SearchForDest();
        }

        if (walkpointSet)
        {
            agent.SetDestination(destPoint);
            if (Vector3.Distance(transform.position, destPoint) < 1) 
            {
                walkpointSet = false;
            }
        }
    }

    void SearchForDest()
    {
        float z = Random.Range(-range, range);
        float x = Random.Range(-range, range);

        destPoint = new Vector3(transform.position.x + x, transform.position.y, transform.position.z + z);

        RaycastHit hit;
        if (Physics.Raycast(destPoint, Vector3.down, out hit, Mathf.Infinity, groundLayer))
        {
            walkpointSet = true;
            destPoint = hit.point;
        }
    }
    
    void EnableAttack()
    {
        boxCollider.enabled = true;
    }
    
    void DisableAttack()
    {
        boxCollider.enabled = false;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Sword")) // ตรวจสอบว่าชนกับดาบหรือไม่
        {
            // ดาบชนเข้ามา
            SwordScript sword = other.GetComponent<SwordScript>();
            if (sword != null && sword.girlAnim.GetBool("Attacking"))
            {
                // ดาบของผู้เล่นกำลังโจมตี
                TakeDamage(sword.hitDamage); // หรือใช้ความเสียหายจากดาบของคุณ
                
                // เรียกใช้ฟังก์ชันใน HealthBar เพื่อแสดงการลดลงของหลอดเลือด
                healthBar.TakeDamage(sword.hitDamage);
            }
        }
    }
    
    public void TakeDamage(int damage)
    {
        if (damage > 0)
        {
            // ทำการลด HP ของ AIEnemy ตามความเสียหายที่ได้รับ
            currentHealth -= damage;

            if (currentHealth <= 0)
            {
                currentHealth = 0;
                // ทำอะไรบางอย่างเมื่อ AIEnemy ตาย
                Destroy(gameObject);
            }

            // อัปเดต HealthBar ของ AIEnemy
            healthBar.SetCurrentHealth(currentHealth);
        }
    }
    
    
    //โชว์เส้น
    private void OnDrawGizmosSelected()
    {
        // Show range as a red wire sphere
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);

        // Show sightRange as a yellow wire sphere
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, cansee);

        // Show attackRange as a green wire sphere
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, attackRange);
    }
    
}


