using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class PlayerController : MonoBehaviourPunCallbacks
{
    [Header("Player")]
    public LifeBarScript lifeBarScript;
    public Transform model;
    public GameObject closeItem;
    public GameObject bloodPrefab;
    public Transform bloodPos;
    public Transform boss;
    public Animator bossAnim;
    
    [Header("Camera")]
    public bool LookCamPos;
    public GameObject CineCamObject;
    public float TopClamp = 70f;
    public float BottomClamp = -30f;
    public float CamOver = 0.0f;
    private float _cineTargetYaw;
    private float _cineTargetPitch;

    private float moveSpeed = 4;
    private Animator anim;
    private RPCAnimation _rpcAnimation;
    private Vector3 stickDirection;
    private Camera mainCamera;

    private CapsuleCollider capsuleCol;
    private Rigidbody rb;

    [Header("Sound")]
    public AudioClip swordDamageSound;

    private float lastDamageTakenTime = 0;

    private Vector3 forwardLocked;
    

    [HideInInspector]
    public bool insideAuraMagic = false;
    [HideInInspector]
    public float swordCurrentDamage; 

    public CameraShaker shaker;

    // Win
    public Transform interactWin; 
    public Text interactText;
    private bool isInteractWin; 


    void Start()
    {
        _rpcAnimation = GetComponent<RPCAnimation>();
        anim = model.GetComponent<Animator>();
        mainCamera = Camera.main;
        capsuleCol = model.GetComponentInChildren<CapsuleCollider>();
        rb = this.GetComponent<Rigidbody>();
        _cineTargetYaw = CineCamObject.transform.rotation.eulerAngles.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManagerScript.isBossDead) anim.SetBool("LockedCamera", false); 

        stickDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        if (anim.GetBool("Equipped")) moveSpeed = 4.5f; 
        else moveSpeed = 6; 

        if (anim.GetBool("Drinking")) moveSpeed = 2; 

        //if (anim.GetBool("Dead") || anim.GetCurrentAnimatorStateInfo(2).IsName("Sweep Fall") || anim.GetCurrentAnimatorStateInfo(2).IsName("Getting Thrown")) return; 

        if (insideAuraMagic || GameManagerScript.gameIsPaused) 
        {
            anim.SetFloat("Speed", 0);
            anim.SetFloat("Horizontal", 0);
            anim.SetFloat("Vertical", 0);

            object[] data = new object[]
            {
                "Speed", 0,
                "Horizontal", 0,
                "Vertical", 0
            };
            _rpcAnimation.AnimationMovementRPC(data);
            return;
        }

        WinInteraction();

    //    if (!anim.GetCurrentAnimatorStateInfo(2).IsName("None") || isBonfireLit) return;

        Move(); //PunRPC
        Rotation();
        Attack();  //PunRPC
        Dodge();  //PunRPC

        // Send movement data to other players
        /* if (photonView.IsMine)
         {
             SendMovementData(transform.position);
         }*/
    }
    /*
    //Multi
    [PunRPC]
    private void UpdatePlayerPosition(Vector3 position)
    {
        transform.position = position;
    }

    private void SendMovementData(Vector3 position)
    {
        photonView.RPC("UpdatePlayerPosition", RpcTarget.Others, position);
    }*/

    private void LateUpdate()
    {
        CameraRotation(); //หมุนก้อง
    }

    public void DisableWin() //หลังชนะบอส
    {
        closeItem.SetActive(false);
    }
    
    private void WinInteraction()
    {
        if (Vector3.Distance(model.transform.position, interactWin.position) < 2.5f && interactWin.gameObject.activeSelf)
        {
            interactText.gameObject.SetActive(!isInteractWin);
            if (Input.GetKeyDown(KeyCode.E) && !isInteractWin)
            {
                anim.SetTrigger("LightBonfire");
                isInteractWin = true;
            }
        }
        else interactText.gameObject.SetActive(false);
    }

    private void Move()
    {
        float x = mainCamera.transform.TransformDirection(stickDirection).x;
        float z = mainCamera.transform.TransformDirection(stickDirection).z;
        if (x > 1) x = 1; 
        if (z > 1) z = 1;

        if (anim.GetBool("CanMove"))
        {
            if(Mathf.Abs(anim.GetFloat("Speed")) > 0.15f)
                model.position += new Vector3(x * moveSpeed * Time.deltaTime, 0, z * moveSpeed * Time.deltaTime); 
            float clampValue = 1; //Input.GetKey(KeyCode.Space) ? 1 : 0.35f; 
            
            float speed = Vector3.ClampMagnitude(stickDirection, clampValue).magnitude;
            float dampTime = 0.02f; 
            
            anim.SetFloat("Speed", speed, dampTime, Time.deltaTime);
            anim.SetFloat("Horizontal", stickDirection.x); // lockedCamera
            anim.SetFloat("Vertical", stickDirection.z); // lockedCamera
            
            //RPC
            object[] data = new object[]
            {
                "Speed", speed, dampTime,
                "Horizontal", stickDirection.x,
                "Vertical", stickDirection.z
            };
            _rpcAnimation.AnimationMovementRPC(data);
            
            if (anim.GetBool("Drinking") && anim.GetFloat("Speed") > 0.25f) anim.SetFloat("Speed", 0.25f); 
            if (anim.GetBool("Drinking") && anim.GetFloat("Vertical") > 0.25f) anim.SetFloat("Vertical", 0.25f);
        }
        
    }

    private void DodgeController() //locked camera
    {
        Vector3 relativeForward = mainCamera.transform.TransformDirection(Vector3.forward);
        Vector3 relativeRight = mainCamera.transform.TransformDirection(Vector3.right);
        Vector3 relativeLeft = mainCamera.transform.TransformDirection(-Vector3.right);
        Vector3 relativeBack = mainCamera.transform.TransformDirection(-Vector3.forward) * 5;

        relativeForward.y = 0;
        relativeRight.y = 0;
        relativeLeft.y = 0;
        relativeBack.y = 0;

        if (Input.GetAxis("Horizontal") > 0.1f && Input.GetAxis("Vertical") > 0.1f && InputManager.GetDodgeInput())
        {
            forwardLocked = (relativeForward + relativeRight).normalized;
        }
        else if (Input.GetAxis("Horizontal") > 0.1f && Input.GetAxis("Vertical") < -0.1f && InputManager.GetDodgeInput())
        {
            forwardLocked = (relativeBack + relativeRight).normalized;
        }
        else if (Input.GetAxis("Horizontal") < -0.1f && Input.GetAxis("Vertical") < -0.1f && InputManager.GetDodgeInput())
        {
            forwardLocked = (relativeBack + relativeLeft).normalized;
        }
        else if (Input.GetAxis("Horizontal") < -0.1f && Input.GetAxis("Vertical") > 0.1f && InputManager.GetDodgeInput())
        {
            forwardLocked = (relativeForward + relativeLeft).normalized;
        }

        else if (Input.GetAxis("Horizontal") > 0.1f && InputManager.GetDodgeInput())
        {
            forwardLocked = relativeRight;
        }
        else if (Input.GetAxis("Horizontal") < -0.1f && InputManager.GetDodgeInput())
        {
            forwardLocked = relativeLeft;
        }
        else if (Input.GetAxis("Vertical") > 0.1f && InputManager.GetDodgeInput())
        {
            forwardLocked = relativeForward;
        }
        else if (Input.GetAxis("Vertical") < -0.1f && InputManager.GetDodgeInput())
        {
            forwardLocked = relativeBack;
        }
        else if(!anim.GetBool("Dodging"))
        {
//            forwardLocked = targetLock.position - model.position;
            forwardLocked.y = 0;
        }
    }

    private void CameraRotation() //ทำกล้อง
    {
        Vector2 _input = Mouse.current.delta.value;
        
        if (_input.sqrMagnitude >= 0.01f && !LookCamPos)
        { 
            float deltaTimeMulti = 0.2f;  // ปรับความเร็วในการหมุนตามความต้องการ

            _cineTargetYaw += _input.x * deltaTimeMulti;
            _cineTargetPitch += _input.y * deltaTimeMulti;
        }

        _cineTargetYaw = ClampAngle(_cineTargetYaw, float.MinValue, float.MaxValue);
        _cineTargetPitch = ClampAngle(_cineTargetPitch, BottomClamp, TopClamp);
        
        CineCamObject.transform.rotation = Quaternion.Euler(_cineTargetPitch + CamOver, _cineTargetYaw, 0.0f);
    }

    static float ClampAngle(float lfAngle, float lfMin, float lfMax)
    {
        if (lfAngle < -360f) lfAngle += 360;
        if (lfAngle > 360) lfAngle -= 360;
        return Mathf.Clamp(lfAngle, lfMin, lfMax);
    }
    
    private void Rotation()
    {
        if (anim.GetBool("Attacking")) return; 

        if (!anim.GetBool("LockedCamera")) // camera livre
        {
            Vector3 rotationOffset = mainCamera.transform.TransformDirection(stickDirection) * 4f;
            rotationOffset.y = 0;
            model.forward += Vector3.Lerp(model.forward, rotationOffset, Time.deltaTime * 30f);
        }
        else // camera locked
        {
            //Vector3 rotationOffset = targetLock.position - model.position;
            //rotationOffset.y = 0;

            DodgeController(); //หันไปทางด้านข้างของการหลบทันที
            model.forward += Vector3.Lerp(model.forward, forwardLocked, Time.deltaTime * 20f);
        }

    }

    private void Attack()
    {
        if (InputManager.GetPrimaryAttackInput() && anim.GetBool("CanAttack") && anim.GetBool("Equipped") && !anim.GetBool("Drinking")) // การโจมตีหลัก
        {
            anim.SetTrigger("LightAttack");
            
            //RPC
            object[] data = new object[]
            {
                "LightAttack", true
            };
            _rpcAnimation.AnimationAttackRPC(data);
        }
        if (InputManager.GetSecondaryAttackInput() && anim.GetBool("CanAttack") && anim.GetBool("Equipped") && !anim.GetBool("Drinking")) // การโจมตีรอง
        {
            anim.SetTrigger("HeavyAttack");
            
            //RPC
            object[] data = new object[]
            {
                "HeavyAttack", true
            };
            _rpcAnimation.AnimationAttackRPC(data);
        }

        if (InputManager.GetDrawSwordInput()) // ปุ่มกลางของเมาส์
        {
            anim.SetTrigger("Weapon");
            
            //RPC
            object[] data = new object[]
            {
                "Weapon", true
            };
            _rpcAnimation.AnimationAttackRPC(data);
        }

        if (InputManager.GetCameraInput() && !GameManagerScript.isBossDead) // เข้าและออกจากโหมดกล้องต่อสู้ ยังต้องแก้
        {
            if(anim.GetBool("Equipped"))
                anim.SetBool("LockedCamera", !anim.GetBool("LockedCamera"));
            
        }
    } //เช็คอนิเมชั่น
    
    private void Dodge()
    {
        //Vector3 diff = model.transform.eulerAngles - mainCamera.transform.eulerAngles;

        if (InputManager.GetDodgeInput() && CanDodge()) // ถ้าผู้เล่นกดปุ่มหลบหลีกและสามารถหลบหลีกได้
        {
            anim.SetTrigger("Dodge");
            
            //RPC
            object[] data = new object[]
            {
                "Dodge", true
            };
            _rpcAnimation.AnimationDodgeRPC(data);
        }
    } //หลบหลีก

    private bool CanMove()
    {
        return anim.GetCurrentAnimatorStateInfo(2).IsName("None");
    } //การเคลื่อนที่

    private bool CanDodge() // เช็คว่าสามารถหลบได้ไหม
    {
        return !anim.GetBool("Attacking") && !anim.GetBool("Drinking") && !anim.GetCurrentAnimatorStateInfo(1).IsName("Sprinting Forward Roll");
    }

    private void OnParticleCollision(GameObject other)
    {
        if(other.gameObject.name.Contains("Shock") && !anim.GetBool("Intangible"))
        {
            RegisterDamage(4);
            return;
        }

        if (other.transform.root.name.Contains("Earth") && !anim.GetBool("Intangible"))
        {
            RegisterDamage(4.2f);
            return;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name.Contains("AuraMagic"))
            insideAuraMagic = true;
    }

    public void RegisterDamage(float damageAmount)
    {
        Debug.Log("RegisterDamage");
        if (damageAmount == 0 || anim.GetBool("Intangible") || /*!DamageInterval() ||*/ bossAnim.GetBool("Dead")) return;

        anim.SetFloat("Speed", 0);
        anim.SetFloat("Vertical", 0);
        anim.SetFloat("Horizontal", 0);
        lastDamageTakenTime = Time.time; 
        capsuleCol.isTrigger = true;
        rb.isKinematic = true;
        //anim.SetBool("Intangible", true);
        anim.SetBool("CanMove", false);
        
        lifeBarScript.UpdateLife(-damageAmount); 
        DamageAnimation(damageAmount);
        shaker.ShakeCamera(0.3f);

        GameObject blood = Instantiate(bloodPrefab, bloodPos.position, Quaternion.identity);
        blood.transform.LookAt(boss.position);
        Destroy(blood, 0.2f);
    }

    private void DamageAnimation(float damageAmount)
    {
        Debug.Log("Damage");
        if (damageAmount >= 4) // หากความเสียหายรุนแรงเกินไป ให้ทำให้ผู้เล่นล้มลง
        {
            Debug.Log("Damage1");
            Vector3 dir = (boss.transform.position - model.transform.position).normalized; //หันไปหาboss
            float dot = Vector3.Dot(dir, model.transform.forward);

            if(dot >= 0) 
                anim.SetTrigger("FallDamage");
            else if (dot < 0) 
                anim.SetTrigger("FallForward");
            return;
        }

        switch (Random.Range(0, 3)) // ความเสียหายเล็กน้อย 
        {
            case 0:
                anim.SetTrigger("TakeDamage");
                Debug.Log("Damage case0");
                break;
            case 1:
                anim.SetTrigger("TakeDamageLeft");
                Debug.Log("Damage case1");
                break;
            case 2:
                anim.SetTrigger("TakeDamageRight");
                Debug.Log("Damage case2");
                break;
        }
        
    }
 
    public void TakeDamage(float amount)
    {
        lifeBarScript.UpdateLife(-0.5f); // ให้ Girl ได้รับความเสียหาย -1
        anim.SetTrigger("TakeDamage");
    } 
    

}
