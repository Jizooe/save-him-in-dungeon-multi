using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDealer : MonoBehaviour
{
    public bool damageOn; // ความเสียหายเปิดใช้งานอยู่
    public float damageAmount; // จำนวนความเสียหายที่จะเกิดขึ้นกับผู้เล่น
    public AudioClip[] impactSound; 

    private float lastSoundTime = 0;

    public float GetDamage() 
    {
        return damageAmount;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!damageOn) return; // ส่งคืนหากไม่ทำให้เกิดความเสียหาย

        if (/*other.gameObject.layer != 9 && */other.gameObject.layer != 11 && other.gameObject.layer != 13) return;

        if (other.gameObject.name == "Player -Skin1") // Player1 ตามชื่อ
        {
            if (other.GetComponent<Animator>().GetBool("Intangible")) return; 
            other.transform.GetComponentInParent<PlayerController>().RegisterDamage(damageAmount); // สร้างความเสียหายให้กับผู้เล่น
        }
        
        if (other.gameObject.name == "Player2 - Skin") // Player2
        {
            if (other.GetComponent<Animator>().GetBool("Intangible")) return; 
            other.transform.GetComponentInParent<Player2Controller>().RegisterDamage(damageAmount); 
        }

        if (SoundInterval() && impactSound.Length > 0) 
        {
            lastSoundTime = Time.time;
        }
    }

    public void GreatSwordFiller(GameObject other)
    {
        if (!damageOn) return; 

        if (other.gameObject.layer != 11 && other.gameObject.layer != 13) return; 

        if (other.gameObject.name == "Player -Skin1") // Player1
        {
            if (other.GetComponent<Animator>().GetBool("Intangible")) return; 
            other.transform.GetComponentInParent<PlayerController>().RegisterDamage(damageAmount); 
        }
        
        if (other.gameObject.name == "Player2 - Skin") // Player2
        {
            if (other.GetComponent<Animator>().GetBool("Intangible")) return; 
            other.transform.GetComponentInParent<Player2Controller>().RegisterDamage(damageAmount); 
        }

        if (SoundInterval() && impactSound.Length > 0) 
        {
            lastSoundTime = Time.time;
        }
    }

    private bool SoundInterval()
    {
        return Time.time > lastSoundTime + 0.5f;
    }

}
