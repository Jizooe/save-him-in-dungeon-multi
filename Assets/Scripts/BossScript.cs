using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

public class BossScript : MonoBehaviour
{
    public Transform model;
    public Transform greatSword;

    private Animator anim;
    public Transform player;
    public PlayerController playercontroller;
    public Player2Controller player2controller;

    public AudioClip[] takeDamageSound;
    public BossLifeBarScript bossLifeScript;

    public GameObject hitCounterParent;
    public GameObject bloodPrefab;
    public Transform bloodPos;

    private float rotationSpeed = 6;

    private float lastDamageTakenTime = 0;

    // Hit Counter
    private int hit = 0; // ตี
    private int currentHit = 0; // combo 
    public TextMeshProUGUI hitCounterText;
    public TextMeshProUGUI hitAdderText;

    private BossAttacks _bossAttacks;
    
    
    void Start()
    {
        anim = model.GetComponent<Animator>();
        _bossAttacks = GetComponent<BossAttacks>();
    }

    void Update()
    {
        if (player == null)
        {
            player = _bossAttacks.Bossdistance(); //เรียกหา ตำแหน่ง player จากในสคริปBossAttcks
        }
        if (anim.GetBool("Dead")) return;

        if (anim.GetCurrentAnimatorStateInfo(0).IsName("idle") && anim.GetCurrentAnimatorStateInfo(1).IsName("None") ||
            !anim.GetBool("CanRotate"))
        {
                Vector3 rotationOffset = player.transform.position - model.position;
                rotationOffset.y = 0;
                float lookDirection = Vector3.SignedAngle(model.forward, rotationOffset, Vector3.up);
                anim.SetFloat("LookDirection", lookDirection);
        }
        else if (!anim.GetBool("Attacking") && anim.GetBool("CanRotate"))
        {
            if (player != null) //เพิ่มมา คู่กับ Gamemanager
            {
                var targetRotation = Quaternion.LookRotation(player.transform.position - model.transform.position);

                // ค่อยๆหมุนไปยังจุดเป้าหมาย
                model.transform.rotation = Quaternion.Slerp(model.transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
            }
        }

        model.transform.eulerAngles = new Vector3(0, model.transform.eulerAngles.y, 0);
    }

    private void GreatSwordCollider(bool b) // ตั้งค่า GreatSword โดยเปิด Collider ขณะโจมตี
    {
        greatSword.GetComponent<BoxCollider>().isTrigger = !b;
    }

    private void OnTriggerEnter(Collider other) // // ผู้เล่นโจมตีบอสแต่บอสไม่ได้โจมตี
    {
        if(other.gameObject.tag == "Sword" && other.gameObject.GetComponentInParent<Animator>().GetBool("Attacking") && !anim.GetBool("Attacking") && DamageInterval() && !anim.GetBool("Dead"))
        {
            lastDamageTakenTime = Time.time;
            CreateAndPlay(takeDamageSound[UnityEngine.Random.Range(0, takeDamageSound.Length)], 2);
            StopAllCoroutines(); 
            StartCoroutine(ShowHitCounter()); 
            if (!anim.GetBool("TakingDamage") && !anim.GetBool("Attacking") && anim.GetBool("NotAttacking"))
                anim.SetTrigger("TakeDamage");

            GameObject blood = Instantiate(bloodPrefab, bloodPos.position, Quaternion.identity);
            blood.transform.LookAt(player.position);
            Destroy(blood, 0.2f);
        }
    }

    public void RegisterPlayerSwordFillDamage()
    {
        if (!anim.GetBool("Attacking") && DamageInterval() && !anim.GetBool("Dead")) 
        {
            lastDamageTakenTime = Time.time;
            CreateAndPlay(takeDamageSound[UnityEngine.Random.Range(0, takeDamageSound.Length)], 2);
            StopAllCoroutines(); 
            StartCoroutine(ShowHitCounter()); 
            if (!anim.GetBool("TakingDamage") && !anim.GetBool("Attacking") && anim.GetBool("NotAttacking")) 
                anim.SetTrigger("TakeDamage"); 

            GameObject blood = Instantiate(bloodPrefab, bloodPos.position, Quaternion.identity);
            blood.transform.LookAt(player.position);
            Destroy(blood, 0.2f);
        }
    }

    public void SwordHit(int hit) //โชว์Hit
    {
        this.hit = hit;
        if (hit == 0)
            ClearCurrentHit();
    }

    public void ClearCurrentHit() // combo Hit
    {
        this.currentHit = 0;
    }

    public void HitManager() // เงื่นไขของcombo
    {
        if (currentHit == 0 && hit == 4) // โจมตีสองครั้ง โจมตีครั้งเดียว
        {
            hitCounterText.text = "1 Hit";
            hitAdderText.text = "+50%";
            bossLifeScript.UpdateLife(-1.5f);
            return;
        }

        currentHit++; 

        if (hit == 1) currentHit = 1; // การโจมตีครั้งแรกจะถูกโจมตี 1 เสมอ

        if (currentHit == 1 && hit == 1) // ตีครั้งเดียว
        {
            hitCounterText.text = "1 Hit";
            hitAdderText.text = " ";
            bossLifeScript.UpdateLife(-1);
        }
        else if (currentHit == 1 && hit == 4) // คอมโบสองเท่าของการโจมตีธรรมดาและขวา
        {
            hitCounterText.text = "2 Hits";
            hitAdderText.text = "+75%";
            bossLifeScript.UpdateLife(-1.75f);
        }
        else if (currentHit == 0 && hit == 4) // โจมตีครั้งเดียวจากการโจมตีทางขวา
        {
            hitCounterText.text = "1 Hit";
            hitAdderText.text = "+50%";
            bossLifeScript.UpdateLife(-1.5f);
        }

        if (currentHit == 2 && hit == 2) // การโจมตีครั้งที่สองของคอมโบธรรมดา
        {
            hitCounterText.text = "2 Hits";
            hitAdderText.text = "+50%";
            bossLifeScript.UpdateLife(-1.5f);
        }
        else if (currentHit == 2 && hit == 4) // การโจมตีที่รุนแรงสิ้นสุดคอมโบคู่
        {
            hitCounterText.text = "2 Hits";
            hitAdderText.text = "+75%";
            bossLifeScript.UpdateLife(-1.75f);
        }

        if (currentHit == 3 && hit == 3) // คอมโบของการโจมตีง่ายๆ 3 ครั้ง
        {
            hitCounterText.text = "3 Hits";
            hitAdderText.text = "+75%";
            bossLifeScript.UpdateLife(-1.75f);
        }
        else if (currentHit == 3 && hit == 4) // โจมตีสองครั้งเพื่อจบTripperคอมโบ
        {
            hitCounterText.text = "3 Hits";
            hitAdderText.text = "+100%";
            bossLifeScript.UpdateLife(-2f);
        }

        if (currentHit == 4) // การโจมตีสองครั้งสิ้นสุดคอมโบสี่เท่า
        {
            hitCounterText.text = "4 Hits";
            hitAdderText.text = "+150%";
            bossLifeScript.UpdateLife(-2.5f);
        }
    }

    private bool DamageInterval() // เวลาของความเสียหายกับเวลา
    {
        return (Time.time > lastDamageTakenTime + 0.7f);
    }

    IEnumerator ShowHitCounter()
    {
        HitManager(); 
        hitCounterParent.SetActive(true); // โชว์ข้อความ
        yield return new WaitForSeconds(2);
        hitCounterParent.SetActive(false);
    }

    private void CreateAndPlay(AudioClip clip, float destructionTime, float volume = 1f) // เสียง
    {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = clip;
        audioSource.volume = volume;
        audioSource.Play();
        Destroy(audioSource, destructionTime);
    }

}
