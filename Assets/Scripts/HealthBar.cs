using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
   public Slider healthBar;

   private void Start()
   {
      healthBar = GetComponent<Slider>();
   }
   
   public void SetMaxHealth(int maxHealth)
   {
      healthBar.value = maxHealth;
   }

   public void SetCurrentHealth(int currentHealth)
   {
      healthBar.value = currentHealth;
   }
   
   public void TakeDamage(int damage)
   {
      // ลดค่าหลอดเลือดตามความเสียหายที่ได้รับ
      healthBar.value -= (float)damage / healthBar.maxValue;
   }
}



