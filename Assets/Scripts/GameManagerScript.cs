using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;
using System.Runtime.InteropServices;
using System;

[DefaultExecutionOrder(0)]
public class GameManagerScript : DontDestroy_Singleton<GameManagerScript>
{
    static extern bool SetCursorPos(int X, int Y);

    // Control
    public bool master; 
    public bool resetCacheOnStart;
    [HideInInspector]
    public bool music; 

    public static bool gameHasStarted; 
    public static bool isBossDead; 
    [HideInInspector]
    public bool isAutoRestartOn;
    
    private bool restarting;

    // pause screen
    public GameObject pauseScreen;
    public static bool gameIsPaused = false;

    // Sword
    public SwordScript swordScript;
    public GreatSwordScript greatSwordScript;

    public bool playerIsDead;
    private float keyInterval;
    
    // Player reference
    public List<Transform> player = new List<Transform>();
    public List<Transform> Enemy = new List<Transform>();
    public Transform myPlayerRoom; //เจ้าของห้อง
    
    public override void OnAwake()
    {
        base.OnAwake();
        
        if(resetCacheOnStart) PlayerPrefs.DeleteAll();

        HideCursor(true);
        
    }

    void Start()
    {
        isBossDead = false;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Q) && Input.GetKey(KeyCode.Keypad4) && Input.GetKey(KeyCode.Keypad2) && Time.time - keyInterval > 0.5f)
        {
            keyInterval = Time.time;
            master = !master;
            print("Master: " + master);
        }

        if(Input.GetKey(KeyCode.Q) && Input.GetKey(KeyCode.Backspace) && Time.time - keyInterval > 0.5f)
        {
            keyInterval = Time.time;
            PlayerPrefs.DeleteAll();
        }

        if (Input.GetKeyDown(KeyCode.KeypadEnter) && master && Time.timeScale > 0.5f) Time.timeScale = 0.1f;
        else if (Input.GetKeyDown(KeyCode.KeypadEnter) && master && Time.timeScale < 1) Time.timeScale = 1f;
    }

    public void Restart()
    {
        restarting = true;
    }
    

    public static void HideCursor(bool b)
    {
        if (b) // hide
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else // visible
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            int xPos = Screen.width/2, yPos = Screen.height/3;
            SetCursorPos(xPos, yPos);//กำหนดตำแหน่งเมาส์
        }
    }
    

    private void PauseManager()
    {
        if (!pauseScreen.activeSelf) // pause
        {
            HideCursor(true);
            pauseScreen.SetActive(true);
        }

        //Time.timeScale = Time.timeScale == 1 ? Time.timeScale = 0 : Time.timeScale = 1;
    }
    
    void OnApplicationQuit()
    {
//        Debug.Log("Application ending after " + Time.time + " seconds");
        int totalTime = (int)(PlayerPrefs.GetInt("TotalTime") + Time.time); 
        PlayerPrefs.SetInt("TotalTime", totalTime); 
    }

}
