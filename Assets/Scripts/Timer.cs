using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI timeText;

    private float m_timer;
    
    // Update is called once per frame
    void Update()
    {
        m_timer += Time.deltaTime;
        int minuts = Mathf.FloorToInt(m_timer / 60);
        int seconds = Mathf.FloorToInt(m_timer % 60);
        timeText.text = string.Format("{0:00}:{1:00}", minuts, seconds);
    }
}
