using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class RPCAnimationBoss : MonoBehaviourPun
{
    private Animator _animator;
    private BossAttackReceive _bossAttackReceive;
    
    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _bossAttackReceive = GetComponent<BossAttackReceive>();
    }
    
    private void Start()
    {
        CallRPC(photonView.ViewID);
    }
    
    public void CallRPC(int viewID)
    {
        object[] data = new object[]
        {
            1212312121,
            viewID
        };
        photonView.RPC("RPCMethod", RpcTarget.Others, data);
    }
    
    [PunRPC]
    public void RPCMethod(object[] data)
    {
        print($"Number : {(int)data[0]}");
        print($"ViewID : {(int)data[1]}");
    }
    
    public void AnimationMovementBossRPC(object[] animationData)
    {
        photonView.RPC("RPCAnimationMovementBoss", RpcTarget.Others, animationData);
    }

    [PunRPC]
    public void RPCAnimationMovementBoss(object[] animationData) //เดิน
    {
        _animator.SetFloat((string)animationData[0], (float)animationData[1]);
        _animator.SetFloat((string)animationData[2], (float)animationData[3]);
    }
    
    public void AnimationAttackBossRPC(string animationData)
    {
        photonView.RPC("RPCAnimationAttackBoss", RpcTarget.Others, animationData);
    }
        
    [PunRPC]
    public void RPCAnimationAttackBoss(string animationData)  //Attack()
    {
        _animator.SetTrigger(animationData[0]); // เรียกใช้งาน Trigger ใน Animator
    }
    
    
    public void AnimationBoolBossRPC(object[] animationData)
    {
        photonView.RPC("RPCAnimationBoolBoss", RpcTarget.Others, animationData);
    }
        
    [PunRPC]
    public void RPCAnimationBoolBoss(object[] animationData)  
    {
        _animator.SetBool((string)animationData[0], (bool)animationData[1]); // เรียกใช้งาน Bool ใน Animator
    }
    
    
    public void AnimationStringRPC(object[] animationData)
    {
        photonView.RPC("RPCAnimationString", RpcTarget.Others, animationData);
    }
        
    [PunRPC]
    public void RPCAnimationString(string animationData)  
    {
        
    }
    
    public void AnimationByteRPC(int skill)
    {
        photonView.RPC("RPCAnimationByte", RpcTarget.Others, skill);
    }
        
    [PunRPC]
    public void RPCAnimationByte(int skill)  
    {
        //(AttackBoss)skill 
        _bossAttackReceive.skillFarAttack(skill);
        _bossAttackReceive.skillNearAttack(skill);
    }
    
}
