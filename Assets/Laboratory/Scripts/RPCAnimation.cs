﻿using System;
using Photon.Pun;
using UnityEngine;

    public class RPCAnimation : MonoBehaviourPun
    {
        private Animator _animator;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        private void Start()
        {
            CallRPC(photonView.ViewID);
        }

        public void CallRPC(int viewID)
        {
            object[] data = new object[]
            {
                1212312121,
                viewID
            };
            photonView.RPC("RPCMethod", RpcTarget.Others, data);
        }

        [PunRPC]
        public void RPCMethod(object[] data)
        {
            print($"Number : {(int)data[0]}");
            print($"ViewID : {(int)data[1]}");
        }

        public void AnimationMovementRPC(object[] animationData)
        {
            photonView.RPC("RPCAnimationMovement", RpcTarget.Others, animationData);
        }

        [PunRPC]
        public void RPCAnimationMovement(object[] animationData) //เดิน
        {
            _animator.SetFloat((string)animationData[0], (float)animationData[1], (float)animationData[2], Time.deltaTime);
            _animator.SetFloat((string)animationData[3], (float)animationData[4]);
            _animator.SetFloat((string)animationData[5], (float)animationData[6]);
        }

        public void AnimationDodgeRPC(object[] animationData)
        {
            photonView.RPC("RPCAnimationDodge", RpcTarget.Others, animationData);
        }
        
        [PunRPC]
        public void RPCAnimationDodge(object[] animationData)  //กลิ้ง
        {
            _animator.SetTrigger((string)animationData[0]); // เรียกใช้งาน Trigger ใน Animator
        }
        
        public void AnimationAttackRPC(object[] animationData)
        {
            photonView.RPC("RPCAnimationAttack", RpcTarget.Others, animationData);
        }
        
        [PunRPC]
        public void RPCAnimationAttack(object[] animationData)  //Attack()
        {
            _animator.SetTrigger((string)animationData[0]); // เรียกใช้งาน Trigger ใน Animator
        }
        
/*
        public void AnimationJumpRPC(object[] animationData)
        {
            photonView.RPC("RPCAnimationJump", RpcTarget.Others, animationData);
        }

        [PunRPC]
        public void RPCAnimationJump(object[] animationData)
        {
            _animator.SetBool((int)animationData[0], (bool)animationData[1]);
        }
        
        public void AnimationFreeFallRPC(object[] animationData)
        {
            photonView.RPC("RPCAnimationFreeFall", RpcTarget.Others, animationData);
        }

        [PunRPC]
        public void RPCAnimationFreeFall(object[] animationData)
        {
            _animator.SetBool((int)animationData[0], (bool)animationData[1]);
        }
        
        public void AnimationGroundedRPC(object[] animationData)
        {
            photonView.RPC("RPCAnimationGrounded", RpcTarget.Others, animationData);
        }

        [PunRPC]
        public void RPCAnimationGrounded(object[] animationData)
        {
            _animator.SetBool((int)animationData[0], (bool)animationData[1]);
        }*/
    }