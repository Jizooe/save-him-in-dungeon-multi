using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;
using Cinemachine;
using Photon.Pun.UtilityScripts;
using UnityEngine.InputSystem;

public class PunNetworkManager : ConnectAndJoinRandom
{
    public static PunNetworkManager singleton;

    public CinemachineVirtualCamera _vCam;
    public PlayerController _playerController;

    [Header("Spawn Info")]
    [Tooltip("The prefab to use for representing the player")]
    public GameObject GamePlayerPrefab;

    public Transform boss;
    public GameObject BossPrefab;

    private GameManagerScript _gameManagerScript;

    private void Awake()
    {
        singleton = this;
    }
    

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        Debug.Log("New Player. " + newPlayer.ToString());
        
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        if (PunUserNetControl.LocalPlayerInstance == null)
        {
            Debug.Log("We are Instantiating LocalPlayer from " + SceneManagerHelper.ActiveSceneName);
            PunNetworkManager.singleton.SpawnPlayer();
        }
        else
        {
            Debug.Log("Ignoring scene load for " + SceneManagerHelper.ActiveSceneName);
        }
    }

    public void SpawnPlayer()
    {
        // we're in a room. spawn a character for the local player.
        // it gets synced by using PhotonNetwork.Instantiate
        _playerController = GetComponent<PlayerController>();
        PhotonNetwork.Instantiate(GamePlayerPrefab.name, new Vector3(0f, 0f, 0f), Quaternion.identity, 0);
        
        if(PhotonNetwork.IsMasterClient)
            PhotonNetwork.InstantiateRoomObject(BossPrefab.name, boss.position, boss.rotation, 0); //สร้างตำแหน่งboss
    }
}