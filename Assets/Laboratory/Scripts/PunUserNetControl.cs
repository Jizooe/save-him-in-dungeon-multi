using System;
using Cinemachine;
using UnityEngine;
using Photon.Pun;
using UnityEngine.InputSystem;

[RequireComponent(typeof(PhotonView))]
[RequireComponent(typeof(PhotonTransformView))]
public class PunUserNetControl : MonoBehaviourPunCallbacks, IPunInstantiateMagicCallback
{
    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance;

    public CinemachineVirtualCamera vvvcam;
    public Transform CameraRoot;

    private GameManagerScript _gameManagerScript;
    

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        Debug.Log(info.photonView.Owner.ToString());
        Debug.Log(info.photonView.ViewID.ToString());
        PlayerController _playercontroller = GetComponent<PlayerController>();
        _gameManagerScript = GameManagerScript.Instance;
        
        _gameManagerScript.player.Add(gameObject.transform); // Gamemanager player ตำแหน่ง
        

        // #Important
        // used in PunNetworkManager.cs
        // : we keep track of the localPlayer instance to prevent instanciation
        // when levels are synchronized
        if (photonView.IsMine) //เรียกใช้ ismineให้เห็น
        {
            
            LocalPlayerInstance = gameObject;
            LocalPlayerInstance.name += " master cline";
            //    GetComponentInChildren<MeshRenderer>().material.color = Color.blue; //สี

            // Reference Camera on run-time
            PunNetworkManager.singleton._vCam.Follow = CameraRoot;
            PunNetworkManager.singleton._vCam.LookAt = CameraRoot;//กล้องตาม
            vvvcam = PunNetworkManager.singleton._vCam;
        
        
        
            //add เจ้าของห้อง เจ้าของเซิฟ
            
            //Reference Input on rum-time
        //    PlayerController _playercontroller = GetComponent<PlayerController>();
        //    _playercontroller = PunNetworkManager.singleton._playerController;
            
            
            _playercontroller.enabled = true;
        }
        else
        {
            _playercontroller.enabled = false;
            //GetComponent<PlayerController>().enabled = false;
        }
    }

    private void OnDestroy()
    {
        _gameManagerScript.player.Remove(gameObject.transform);  //ทำลายทิ้ง
    }
}