using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.Events;

[RequireComponent(typeof(PhotonView))]
[RequireComponent(typeof(PhotonTransformView))]
public class PunUserBossNetControl : MonoBehaviourPunCallbacks
{
    //ของBoss PhotonView
    [SerializeField]private List<MonoBehaviour> Test;

    private BossAttacks _bossAttacks;
    
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        _bossAttacks = GetComponent<BossAttacks>();
        if (photonView.IsMine)
        {
            _bossAttacks.Bossdistance(); //เรียกใช้ Bossdistance ใน
        }
        else
        {
            foreach (var test in Test)
            {
                test.enabled = false; //ปิดไว้ ไม่ให้ อีกเครื่องทำงาน ประมวลผลแค่เครื่องเรา
            }
        }
    }
}
