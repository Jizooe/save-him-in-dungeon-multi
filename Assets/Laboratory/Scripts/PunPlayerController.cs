using ExitGames.Client.Photon;
using UnityEngine;
using Photon.Pun;

namespace GDD
{
    public class PunPlayerController : MonoBehaviourPun
    {
        public GameObject bulletPrefab;
        public Camera m_playerCam;
        public GameObject firstaidPrefab;

        void Start()
        {
            if (m_playerCam == null)
                m_playerCam = Camera.main;
        }

        void Update()
        {
            if (!photonView.IsMine)
                return;

#if ENABLE_INPUT_SYSTEM
#endif
        }

    }
}