using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreamEffect : MonoBehaviour
{
    private void Start()
    {
        Destroy(this.gameObject, 3); //กำจัดทิ้งใน 3 วินาที
    }

    private void Update()
    {
        this.transform.localScale += new Vector3(10f, 10f, 10f) * Time.deltaTime; //เพิ่มระยะของวง
    }
}
