using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AuraMagic : MonoBehaviour
{
    private Transform player;
    private Transform player2;
    private bool isEnabled = true;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        player2 = GameObject.FindGameObjectWithTag("Player").transform;
        StartCoroutine(DisableAura());
    }

    // Update is called once per frame
    void Update()
    {
        if (player != null && player2 != null)
        {
            if (Vector3.Distance(this.transform.position, player.position) < 6.0f && isEnabled) // ระยะห่างเวทย์มนตร์ส่งผลต่อPlayer
            {
                PlayerController playerController1 = player.GetComponentInParent<PlayerController>();
                Player2Controller playerController2 = player2.GetComponentInParent<Player2Controller>();

                if (playerController1 != null)
                    playerController1.insideAuraMagic = true;

                if (playerController2 != null)
                    playerController2.insideAuraMagic = true;
            }
            else
            {
                PlayerController playerController1 = player.GetComponentInParent<PlayerController>();
                Player2Controller playerController2 = player2.GetComponentInParent<Player2Controller>();

                if (playerController1 != null)
                    playerController1.insideAuraMagic = false;

                if (playerController2 != null)
                    playerController2.insideAuraMagic = false;
            }
        }
    }


    IEnumerator DisableAura() // 3 วินา ปิดใช้งาน MagicAura
    {
        yield return new WaitForSeconds(3);
        isEnabled = false; // ปิดการใช้งาน
        
        if (player != null)
        {
            PlayerController playerController = player.GetComponentInParent<PlayerController>();
            if (playerController != null)
            {
                playerController.insideAuraMagic = false;
            }
                 
        }

        if (player2 != null)
        {
            Player2Controller player2Controller = player2.GetComponentInParent<Player2Controller>();
            if (player2Controller != null)
            {
                player2Controller.insideAuraMagic = false;
            }
        }
      
        
        Destroy(this.gameObject);
    }
}