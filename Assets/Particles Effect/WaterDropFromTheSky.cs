using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterDropFromTheSky : MonoBehaviour
{
     public float fallSpeed = 20;
     public AudioClip hitGroundSound;
     private bool hasPlayedHitSound = false; //รับรองว่าเสียงจะกระแทกพื้นเพียงครั้งเดียว
    
        private void OnEnable()
        {
            this.gameObject.layer = 12;
            this.transform.GetChild(0).gameObject.layer = 12;
            Destroy(this.gameObject, 1);
            this.transform.eulerAngles = new Vector3(this.transform.eulerAngles.x, Random.Range(0f, 360f), this.transform.eulerAngles.z);
        }
    
        void Update()
        {
            if (this.transform.position.y > 0.5f) //ในขณะที่มันยังไม่ถึงพื้น
            {
                this.transform.position += transform.up * Time.deltaTime * fallSpeed * -1; // แรงโน้มถ่วง
            } 
            else if (!hasPlayedHitSound) // หยุดน้ำเมื่อกระทบพื้นแล้วส่งเสียง
            {
                hasPlayedHitSound = true; //รับรองว่าเสียงจะกระแทกพื้นเพียงครั้งเดียว
        //        CreateAndPlay(hitGroundSound, 1);
                this.GetComponent<DamageDealer>().damageOn = false; // น้ำจะไม่สร้างความเสียหายเมื่ออยู่บนพื้นอีกต่อไป
            }
        }
    /*
        private void CreateAndPlay(AudioClip clip, float destructionTime, float volume = 1f)
        {
            AudioSource audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.clip = clip;
            audioSource.volume = volume;
            audioSource.spatialBlend = 1;
            audioSource.Play();
            Destroy(audioSource, destructionTime);
        }*/
}
