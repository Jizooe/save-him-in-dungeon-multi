using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class SpellProjectile : MonoBehaviour
{
    public GameObject blastPrefab;
    public AudioClip blastSound;
    public LifeBarScript lifeBarScript;
    private Transform player;
    private Transform player2;
    private float speed = 50;
    private float turn = 20;
    private Vector3 offset;

    private float distance;
    private Rigidbody rb;

    private bool chase = true;

    private float lastTime;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    //    player2 = GameObject.FindGameObjectWithTag("Player").transform;
        offset = new Vector3(0, 1f, 0);
        rb = this.GetComponent<Rigidbody>();
        lifeBarScript = GameObject.Find("Canvas").transform.Find("LifeBar Parent").GetChild(0).GetComponent<LifeBarScript>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        distance = (player.transform.position - this.transform.position).sqrMagnitude;

        rb.velocity = transform.forward * speed; //ใช้ความเร็วกับโพรเจกไทล์

        if(distance > 2 && chase) //หากยังไม่ได้ผ่านตัวผู้เล่น
        {
            Quaternion targetRotation = Quaternion.LookRotation((player.position + offset) - transform.position);
            rb.MoveRotation(Quaternion.RotateTowards(transform.rotation, targetRotation, turn));
        } else
        {
            chase = false; // ไม่ไล่ล่าผู้เล่นอีกต่อไป เพียงแค่เดินหน้าต่อไป
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!TimeInterval()) return;
        lastTime = Time.time;
        Instantiate(blastPrefab, this.transform.position, Quaternion.identity); // ระเบิด

        if(other.gameObject.tag == "Player" && !player.GetComponent<Animator>().GetBool("Intangible")) //ถ้ามันถึงตัวผู้เล่น
        {
            lifeBarScript.StartBleeding(); // เลือดเริ่มลด
        }

        Destroy(this.gameObject, 0.1f); // ทำลายวัตถุนี้หลังจากชน
    }

    private bool TimeInterval()
    {
        return Time.time > lastTime + 0.5f;
    }

}